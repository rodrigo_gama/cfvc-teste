/* Root */
const Introducao = () => import('./pages/Introducao')
const GettingStarted = () => import('./pages/GettingStarted')
// const Introduction = (r) => require.ensure([], () => r(require('./pages/Introduction')), 'base');
// const GettingStarted = (r) => require.ensure([], () => r(require('./pages/GettingStarted')), 'base');
// const About = (r) => require.ensure([], () => r(require('./pages/About')), 'base');
// const Changelog = (r) => require.ensure([], () => r(require('./pages/Changelog')), 'base');
// const Error404 = (r) => require.ensure([], () => r(require('./pages/Error')), 'base');

/* Components */
const InputMoney = () => import('./pages/components/InputMoney')
const Select = () => import('./pages/components/Select')
const SelectMesAno = () => import('./pages/components/SelectMesAno')
const MenuLateral = () => import('./pages/components/MenuLateral')
// const Avatar = (r) => require.ensure([], () => r(require('./pages/components/Avatar')), 'avatar');
// const BottomBar = (r) => require.ensure([], () => r(require('./pages/components/BottomBar')), 'bottom-bar');
// const Buttons = (r) => require.ensure([], () => r(require('./pages/components/Buttons')), 'buttons');
// const ButtonToggle = (r) => require.ensure([], () => r(require('./pages/components/ButtonToggle')), 'button-toggle');
// const Card = (r) => require.ensure([], () => r(require('./pages/components/Card')), 'card');
// const Checkbox = (r) => require.ensure([], () => r(require('./pages/components/Checkbox')), 'checkbox');
// const Chips = (r) => require.ensure([], () => r(require('./pages/components/Chips')), 'chips');
// const Dialog = (r) => require.ensure([], () => r(require('./pages/components/Dialog')), 'dialog');
// const FileComponent = (r) => require.ensure([], () => r(require('./pages/components/File')), 'file');
// const Icon = (r) => require.ensure([], () => r(require('./pages/components/Icon')), 'icon');
// const ImageLoader = (r) => require.ensure([], () => r(require('./pages/components/ImageLoader')), 'image-loader');
// const InkRipple = (r) => require.ensure([], () => r(require('./pages/components/InkRipple')), 'ink-ripple');
// const Input = (r) => require.ensure([], () => r(require('./pages/components/Input')), 'input');
// const List = (r) => require.ensure([], () => r(require('./pages/components/List')), 'list');
// const Menu = (r) => require.ensure([], () => r(require('./pages/components/Menu')), 'menu');
// const Onboarding = (r) => require.ensure([], () => r(require('./pages/components/Onboarding')), 'onboarding');
// const Progress = (r) => require.ensure([], () => r(require('./pages/components/Progress')), 'progress');
// const Radio = (r) => require.ensure([], () => r(require('./pages/components/Radio')), 'radio');
// const RatingBar = (r) => require.ensure([], () => r(require('./pages/components/RatingBar')), 'rating-bar');
// const Select = (r) => require.ensure([], () => r(require('./pages/components/Select')), 'select');
// const Sidenav = (r) => require.ensure([], () => r(require('./pages/components/Sidenav')), 'sidenav');
// const Snackbar = (r) => require.ensure([], () => r(require('./pages/components/Snackbar')), 'snackbar');
// const SpeedDial = (r) => require.ensure([], () => r(require('./pages/components/SpeedDial')), 'speed-dial');
// const Spinner = (r) => require.ensure([], () => r(require('./pages/components/Spinner')), 'spinner');
// const Stepper = (r) => require.ensure([], () => r(require('./pages/components/Stepper')), 'stepper');
// const Subheader = (r) => require.ensure([], () => r(require('./pages/components/Subheader')), 'subheader');
// const Switch = (r) => require.ensure([], () => r(require('./pages/components/Switch')), 'switch');
// const Table = (r) => require.ensure([], () => r(require('./pages/components/Table')), 'table');
// const Tabs = (r) => require.ensure([], () => r(require('./pages/components/Tabs')), 'tabs');
// const Toolbar = (r) => require.ensure([], () => r(require('./pages/components/Toolbar')), 'toolbar');
// const Tooltip = (r) => require.ensure([], () => r(require('./pages/components/Tooltip')), 'tooltip');
// const Whiteframe = (r) => require.ensure([], () => r(require('./pages/components/Whiteframe')), 'whiteframe');

/* UI Elements */
// const Typography = (r) => require.ensure([], () => r(require('./pages/ui-elements/Typography')), 'ui-elements');
// const Layout = (r) => require.ensure([], () => r(require('./pages/ui-elements/Layout')), 'ui-elements');

/* Themes */
// const Configuration = (r) => require.ensure([], () => r(require('./pages/themes/Configuration')), 'themes');
// const DynamicThemes = (r) => require.ensure([], () => r(require('./pages/themes/DynamicThemes')), 'themes');

const main = [
    {
        path: '/',
        name: 'introducao',
        component: Introducao
    },
    // {
    //     path: '/',
    //     name: 'introduction',
    //     component: Introduction
    // },
    {
        path: '/getting-started',
        name: 'getting-started',
        component: GettingStarted
    }
    // {
    //     path: '/about',
    //     name: 'about',
    //     component: About
    // },
    // {
    //     path: '/changelog',
    //     name: 'changelog',
    //     component: Changelog
    // }
]

const components = [
    {
        path: '/components/input-money',
        name: 'components:input-money',
        component: InputMoney
    },
    {
        path: '/components/menu-lateral',
        name: 'components:menu-lateral',
        component: MenuLateral
    },
    {
        path: '/components/select',
        name: 'components:select',
        component: Select
    },
    {
        path: '/components/select-mes-ano',
        name: 'components:select-mes-ano',
        component: SelectMesAno
    }
]

// const theme = [
//     {
//         path: '/themes',
//         name: 'themes',
//         redirect: '/themes/configuration'
//     },
//     {
//         path: '/themes/configuration',
//         name: 'themes:configuration',
//         component: Configuration
//     },
//     {
//         path: '/themes/dynamic-themes',
//         name: 'themes:dynamic-themes',
//         component: DynamicThemes
//     }
// ];

// const uiElements = [
//     {
//         path: '/ui-elements',
//         name: 'ui-elements',
//         redirect: '/ui-elements/typography'
//     },
//     {
//         path: '/ui-elements/typography',
//         name: 'ui-elements:typography',
//         component: Typography
//     },
//     {
//         path: '/ui-elements/layout',
//         name: 'ui-elements:layout',
//         component: Layout
//     }
// ];

// const error = [
//     {
//         path: '*',
//         name: 'error',
//         component: Error404
//     }
// ];

export default [].concat(main, components) //, theme, uiElements, error);
