import ContabilizeiLogo from './ContabilizeiLogo.vue'

export default function install(Vue) {
    Vue.component('contabilizei-logo', ContabilizeiLogo)
}
