import moment from 'moment'

/**
 * Formata uma String/Number para data, de acordo com padrões utilizados pelo moment.js
 * @param {String, Number} value
 * @param {String} format - formatos utilizados pelo moment.js
 * @return {String} dataFormatada
 */
function date(value, format) {
    if (value) {
        let strData = (new Date(value)).toUTCString()
        let dataFormatada = moment(strData).format(format || 'DD/MM/YYYY')
        return dataFormatada
    }

    return ''
}

/**
 * Formata uma String/Number para moeda
 * @param {String, Number} value
 * @param {String} currency - moeda R$, $...
 * @return {String}
 */
function currency(value, currency) {
    currency = currency != null ? currency : 'R$'
    let c = 2
    let d = ','
    let t = '.'
    let s = value < 0 ? '-' : ''
    let i = String(parseInt(value = Math.abs(Number(value) || 0).toFixed(c)))
    let j = i.length
    j = i.length > 3 ? j % 3 : 0
    return s + currency +
        (j ? i.substr(0, j) + t : '') + i.substr(j).replace(/(\d{3})(?=\d)/g, '$1' + t) + (c ? d + Math.abs(value - i).toFixed(c).slice(2) : '')
}

/**
 * Formata uma String/Number para decimal
 * @param {String, Number} value
 * @param {String} decimals - quantidade de casas decimais
 * @return {String} numero formatado
 */
function decimal(value, decimals) {
    decimals = decimals !== null ? decimals : 2
    let stringified = Math.abs(value).toFixed(decimals)
    let _int = decimals ? stringified.slice(0, -1 - decimals) : stringified
    let i = _int.length % 3
    let head = i > 0 ? _int.slice(0, i) : ''
    let _float = decimals
        ? stringified.slice(-1 - decimals)
        : ''

    return head + _int.slice(i) + _float
}

export default {
    filters: {
        currency: currency,
        date: date,
        decimal: decimal
    },

    install: function (Vue) {
        Object.keys(this.filters).forEach(function (filter, k) {
            Vue.filter(filter, this.filters[filter])
        }.bind(this))
    }
}
