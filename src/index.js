// atoms
import ContabilizeiLogo from './components/atoms/ContabilizeiLogo/ContabilizeiLogo'
import ContabilizeiLogoType from './components/atoms/CcLogoType/CcLogoType.vue'
import ContabilizeiTitle from './components/atoms/ContabilizeiTitle/ContabilizeiTitle.vue'
import ContabilizeiSelect from './components/atoms/ContabilizeiSelect/ContabilizeiSelect.vue'
import ContabilizeiData from './components/atoms/ContabilizeiData/ContabilizeiData.vue'
import ContabilizeiCnpj from './components/atoms/ContabilizeiCnpj/ContabilizeiCnpj.vue'
import ContabilizeiCpf from './components/atoms/ContabilizeiCpf/ContabilizeiCpf.vue'
import ContabilizeiCampo from './components/atoms/ContabilizeiCampo/ContabilizeiCampo.vue'
import ContabilizeiCampoDecimal from './components/atoms/ContabilizeiCampoDecimal/ContabilizeiCampoDecimal.vue'
import ContabilizeiCheckbox from './components/atoms/ContabilizeiCheckbox/ContabilizeiCheckbox.vue'
import ContabilizeiInputMoney from './components/atoms/ContabilizeiInputMoney/ContabilizeiInputMoney'
import ContabilizeiMultiUpload from './components/atoms/ContabilizeiMultiUpload/ContabilizeiMultiUpload'

// moleculas
import ContabilizeiSelectMesAno from './components/molecules/ContabilizeiSelectMesAno/ContabilizeiSelectMesAno'
import ContabilizeiMenuLateral from './components/molecules/ContabilizeiMenuLateral/ContabilizeiMenuLateral.vue'
import ContabilizeiSocio from './components/molecules/ContabilizeiSocio/ContabilizeiSocio.vue'

// componentes
import ContabilizeiSearch from './components/molecules/ContabilizeiSearch/ContabilizeiSearch'
// import ContabilizeiButton from './components/ContabilizeiButton/ContabilizeiButton.vue'
// import ContabilizeiField from './components/ContabilizeiField/ContabilizeiField.vue'
// import ContabilizeiRadio from './components/ContabilizeiRadio/ContabilizeiRadio.vue'
// import ContabilizeiLogo from './components/ContabilizeiLogo/ContabilizeiLogo.vue'
// import ContabilizeiTable from './components/ContabilizeiTable/ContabilizeiTable.vue'
// import ContabilizeiMenuButton from './components/ContabilizeiMenuButton/ContabilizeiMenuButton.vue'
// import ContabilizeiText from './components/ContabilizeiText/ContabilizeiText.vue'

// filters
import Vue from 'vue'
import filters from './filters'
import Dictionary from './util/validation/dictionary'
import VeeValidate, { Validator } from 'vee-validate'
import CnpjValidator from './util/validation/cnpj/cnpj.validator'
import CpfValidator from './util/validation/cpf/cpf.validator'
import DataValidator from './util/validation/data/data.validator'
import salarioMinimoValidator from './util/validation/salarioMinimo/salarioMinimo.validator'
import br from 'vee-validate/dist/locale/pt_BR'
import moment from 'moment'

Validator.extend('cnpj', CnpjValidator)
Validator.extend('cpf', CpfValidator)
Validator.extend('data', DataValidator)
Validator.extend('salarioMinimo', salarioMinimoValidator)
// Add locale helper.
Validator.addLocale(br)
Validator.installDateTimeValidators(moment)

Vue.use(VeeValidate, {
    locale: 'pt_BR',
    errorBagName: 'veeErrors',
    dictionary: Dictionary
})

var options = {
    ContabilizeiLogo,
    ContabilizeiLogoType,
    ContabilizeiTitle,
    ContabilizeiSelect,
    ContabilizeiData,
    ContabilizeiCnpj,
    ContabilizeiCpf,
    ContabilizeiCampo,
    ContabilizeiCampoDecimal,
    ContabilizeiCheckbox,
    ContabilizeiInputMoney,
    ContabilizeiMultiUpload,

    ContabilizeiSelectMesAno,
    ContabilizeiMenuLateral,
    ContabilizeiSocio,

    ContabilizeiSearch
    // ContabilizeiButton,
    // ContabilizeiField,
    // ContabilizeiRadio,
    // ContabilizeiLogo,
    // ContabilizeiTable,
    // ContabilizeiMenuButton,
    // ContabilizeiText
}

options.install = function(Vue) {
    for (var component in options) {
        var componentInstaller = options[component]

        Vue.component(options[component].name, options[component])

        if (componentInstaller && component !== 'install') {
            Vue.use(componentInstaller)
        }
    }

    // install filters
    filters.install(Vue)
}

export default options
