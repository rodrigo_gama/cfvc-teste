function validate(value) {
    var data = value
    var dia = data.substring(0, 2)
    var mes = data.substring(3, 5)
    var ano = data.substring(6, 10)

    var novaData = new Date(ano, (mes - 1), dia)

    var mesmoDia = parseInt(dia, 10) === parseInt(novaData.getDate())
    var mesmoMes = parseInt(mes, 10) === parseInt(novaData.getMonth()) + 1
    var mesmoAno = parseInt(ano) === parseInt(novaData.getFullYear())

    if (!((mesmoDia) && (mesmoMes) && (mesmoAno))) {
        return false
    }

    var anoInt = parseInt(ano)
    if (anoInt < 1900 || anoInt > 2200) {
        return false
    }

    return true
}

export default validate
