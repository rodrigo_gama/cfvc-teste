import DataValidate from './data.js'
const validator = {
    getMessage(field, args) { // will be added to default English messages.
        return 'Invalid Date'
    },
    validate(value, args) {
        return DataValidate(value)
    }
}
export default validator
