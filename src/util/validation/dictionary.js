const dictionary = {
    'pt_BR': {
        messages: {
            cnpj: () => {
                return 'CNPJ inválido'
            },
            cpf: () => {
                return 'CPF inválido'
            },
            data: () => {
                return 'Data inválida'
            },
            required: (field) => {
                field = field.toUpperCase()
                return `Campo obrigatório`
                // return `O campo ${field} é obrigatório.`
            }
        }
    }
}
export default dictionary
