function validarSalarioMinimo(salarioBase) {
    salarioBase = salarioBase.toString().replace(/\./g, '').replace(/,/g, '.')
    const number = Number(salarioBase.replace(/[^0-9.]+/g, ''))

    return number >= 937.00
}

export default validarSalarioMinimo
