import salarioMinimoValidate from './salarioMinimo.js'

const validator = {
    getMessage(field, args) { // will be added to default English messages.
        return 'Wage informed is lower than the minimum wage'
    },
    validate(value, args) {
        return salarioMinimoValidate(value)
    }
}
export default validator
